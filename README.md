# RpgAPI

Test for The Tribe

## Install

1. Fist clone repo in your environement.

    ```
    git clone https://github.com/Axel-Girard/rpgAPI.git
    cd rpgAPI/
    ```

2. Install dependance

    `npm install`

3. Setup your .env file

    Copy the `.env.example` to `.env` and add your value
    `cp .env.example .env`

4. Setup your database

    In `config/config.conf` change acces to SQL database
    Then create the database
    `npx sequelize-cli db:create`
    Next, play the migration
    `npx sequelize-cli db:migrate`
    You can also populate the database to have some values
    `npx sequelize-cli db:seed:all`

5. Run server

    `npm start`
