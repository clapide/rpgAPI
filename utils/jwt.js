require('dotenv').config();

const jwt = require('jsonwebtoken');

const { JWT_SIGNE_SECRET } =  process.env;

module.exports = {
    // generate a 24h token for user
    generateTokenForUser: (user) => {
        return jwt.sign({
            userId: user.id,
        },
        JWT_SIGNE_SECRET,
        {
            expiresIn: '24h'
        })
    },
    getUserId: (token) => {
        let userId = -1;
        if (token) {
            try {
                let jwtToken = jwt.verify(token, JWT_SIGNE_SECRET);
                if (jwtToken) {
                    userId = jwtToken.userId;
                }
            } catch (err) {
                console.error(err);
            }
        }
        return userId;
    }
}