module.exports = {
    // Check every stats of a character
    checkLvlUp: (character, skillpoint, health, attack, defense, magik) => {
        // character stats can't be negative or be reduce
        if (skillpoint < 0
            || defense < 0 || (character.defense > defense)
            || health < 0 || (character.health > health)
            || attack < 0 || (character.attack > attack)
            || magik < 0 || (character.magik > magik)) {
            return false;
        }
        let differencePoints = 0;
        differencePoints += health - character.health;
        differencePoints += attack - character.attack;
        differencePoints += defense - character.defense;
        differencePoints += magik - character.magik;

        // if the total of all statsup is greater than the number of actual skillpoint their is a problem
        if (differencePoints !== character.skillpoint - skillpoint) {
            return false;
        }

        return true;
    }
}
