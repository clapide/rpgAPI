'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Fight extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Fight.belongsTo(models.Character, {
        onDelete: 'CASCADE',
        foreignKey: 'initiator',
        targetKey: 'id'
      });
      models.Fight.belongsTo(models.Character, {
        onDelete: 'CASCADE',
        foreignKey: 'opponent',
        targetKey: 'id'
      });
      models.Fight.belongsTo(models.Character, {
        onDelete: 'CASCADE',
        foreignKey: 'winner',
        targetKey: 'id'
      });
    }
  };
  Fight.init({
    initiatorHP: DataTypes.INTEGER,
    opponentHP: DataTypes.INTEGER,
    round: DataTypes.INTEGER,
    logs: DataTypes.ARRAY(DataTypes.STRING)
  }, {
    sequelize,
    modelName: 'Fight',
  });
  return Fight;
};