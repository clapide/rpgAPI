'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Character extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Character.belongsTo(models.User, {
        onDelete: 'CASCADE',
        foreignKey: 'UserId',
        targetKey: 'id'
      });
      models.Character.hasMany(models.Fight, {
        foreignKey: 'initiator'
      });
      models.Character.hasMany(models.Fight, {
        foreignKey: 'opponent'
      });
      models.Character.hasMany(models.Fight, {
        foreignKey: 'winner'
      });
    }
  };
  Character.init({
    skillpoint: DataTypes.INTEGER,
    health: DataTypes.INTEGER,
    attack: DataTypes.INTEGER,
    defense: DataTypes.INTEGER,
    magik: DataTypes.INTEGER,
    rank: DataTypes.INTEGER,
    lastLoss: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Character',
  });
  return Character;
};