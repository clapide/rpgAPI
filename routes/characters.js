const express = require('express');
const router = express.Router();
const jwtUtils = require('../utils/jwt');
const Character = require('../models').Character;
const User = require('../models').User;
const characterUtils = require('../utils/character');

// create a new character
router.post('/create', (req, res, next) => {
    const headerAuth = req.headers['authorization'];
    const userId = jwtUtils.getUserId(headerAuth);

    let skillpoint = req.body.skillpoint;
    let health = req.body.health;
    let attack = req.body.attack;
    let defense = req.body.defense;
    let magik = req.body.magik;
    let rank = req.body.rank;

    if (userId < 0) {
        return res.status(403).json({'error': 'Incorrect token'});
    }

    User.findOne({
        where: { id: userId },
        include: [{
            model: Character,
            attributes: ['id']
        }]
    }).then((user) => {
        if(user){
            if (user.Characters.length >= 10) {
                return res.status(409).json({'error': 'user has already the maximum number of character'});
            }
            let vals = {
                skillpoint: (skillpoint ? skillpoint : 12),
                health: (health ? health : 10),
                attack: (attack ? attack : 0),
                defense: (defense ? defense : 0),
                magik: (magik ? magik : 0),
                rank: (rank ? rank : 1),
                UserId: userId
            };
            const newCharacter = Character.create(vals).then((newCharacter) => {
                return res.status(201).json(newCharacter)
            }).catch((err) => {
                console.error(err)
                return res.status(500).json({'error': 'Can\'t create character'})
            })
        } else {
            return res.status(403).json({'error': 'user not found'});
        }
    }).catch((err) => {
        console.error(err)
        return res.status(500).json({'error': 'Error server, please try again later'})
    })
});

// delete a character of the user
router.post('/delete', (req, res, next) => {
    const headerAuth = req.headers['authorization'];
    const userId = jwtUtils.getUserId(headerAuth);

    let characterId = req.body.id;

    if (userId < 0) {
        return res.status(403).json({'error': 'Incorrect token'});
    }

    Character.findOne({
        where: { id: characterId, UserId: userId }
    }).then((character) => {
        if(character){
            character.destroy();
            return res.status(201).json({'info': 'character delete'});
        } else {
            return res.status(403).json({'error': 'character not found'});
        }
    }).catch((err) => {
        console.error(err)
        return res.status(500).json({'error': 'Error server, please try again later'})
    })
});

// list all characters
router.get('/list', (req, res, next) => {
    const headerAuth = req.headers['authorization'];
    const userId = jwtUtils.getUserId(headerAuth);

    if (userId < 0) {
        return res.status(403).json({'error': 'Incorrect token'});
    }

    Character.findAll({
        where: { UserId: userId }
    }).then((characters) => {
        return res.status(201).json(characters)
    }).catch((err) => {
        return res.status(500).json({'error': 'Error server, please try again later'})
    })
});

// retrive a specific character
router.get('/list/:id', (req, res, next) => {
    const headerAuth = req.headers['authorization'];
    const userId = jwtUtils.getUserId(headerAuth);

    const characterId = req.params.id;

    if (userId < 0) {
        return res.status(403).json({'error': 'Incorrect token'});
    }

    Character.findOne({
        where: {
            UserId: userId,
            id: characterId
        }
    }).then((character) => {
        return res.status(201).json(character)
    }).catch((err) => {
        return res.status(500).json({'error': 'Error server, please try again later'})
    })
});

// Set skill points
router.post('/lvlUp', (req, res, next) => {
    const headerAuth = req.headers['authorization'];
    const userId = jwtUtils.getUserId(headerAuth);

    let charId = req.body.id;
    let skillpoint = req.body.skillpoint;
    let health = req.body.health;
    let attack = req.body.attack;
    let defense = req.body.defense;
    let magik = req.body.magik;
    if (userId < 0) {
        return res.status(403).json({'error': 'Incorrect token'});
    }

    User.findOne({
        where: { id: userId },
        include: [{
            model: Character,
            attributes: ['id']
        }]
    }).then((user) => {
        if(user){
            Character.findOne({
                attributes: ['id', 'skillpoint', 'health', 'attack', 'defense', 'magik', 'rank'],
                where: { id: charId }
            }).then((character) => {
                const isOk = characterUtils.checkLvlUp(character, skillpoint, health, attack, defense, magik);
                if (!isOk) {
                    return res.status(403).json({
                        'error': 'Level up impossible: incorrect data provided'
                    });
                }

                character.skillpoint = skillpoint;
                character.health = health;
                character.attack = attack;
                character.defense = defense;
                character.magik = magik;

                character.save().then((updateCharacter) => {
                    if(updateCharacter){
                        return res.status(200).json(updateCharacter);
                    } else {
                        return res.status(403).json({'error': 'Character can\'t update'});
                    }
                }).catch((err) => {
                    console.error(err)
                    return res.status(500).json({'error': err})
                });
            }).catch((err) => {
                console.error(err)
                return res.status(500).json({'error': err})
            })
        } else {
            return res.status(403).json({'error': 'user not found'});
        }
    }).catch((err) => {
        console.error(err)
        return res.status(500).json({'error': 'Error server, please try again later'})
    })
});

// Recreate Character's database
// FOR TESTING PURPOSES ONLY
router.get('/sync', function (req, res) {
    Character.sync({force: true}).then(() => {
        return res.status(201).json({'Succes': "The table for the Character model was just (re)created!"})
    }).catch((err) => {
        return res.status(500).json({'error': 'Error server, please try again later'})
    })
});

module.exports = router;