'use strict';

const express = require('express');
const router = express.Router();
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "LogRocket Express API with Swagger",
      version: "1.0.0",
      description:
        "This is an API application made with Express and documented with Swagger",
      license: {
        name: "Apache License 2.0",
        url: "http://www.apache.org/licenses/",
      },
    },
    servers: [
      {
        url: "http://localhost:3000/",
      },
    ],
  },
  apis: ["./routes/*.js"],
};

const specs = swaggerJsdoc(options);
router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

router.get('/', (req, res, next) => {
  console.log('hello')
  res.status(200);
});

module.exports = router;