const express = require('express');
const router = express.Router();
const jwtUtils = require('../utils/jwt');
const Fight = require('../models').Fight;
const User = require('../models').User;
const Character = require('../models').Character;
const { Op } = require('sequelize');

// create a new fight or not finished one
router.post('/create', (req, res, next) => {
    const headerAuth = req.headers['authorization'];
    const userId = jwtUtils.getUserId(headerAuth);

    const initiatorId = req.body.initiatorId;

    if (userId < 0) {
        return res.status(403).json({'error': 'Incorrect token'});
    }

    User.findOne({
        where: { id: userId },
        include: [{
            model: Character
        }]
    }).then((user) => {
        if(user){
            const initiator = user.Characters.filter(character => character.id == initiatorId)[0];

            Fight.findOne({
                where: {
                    [Op.or]: [
                        { initiator: initiatorId },
                        { opponent: initiatorId }
                    ],
                    winner: {
                        [Op.eq]: null
                    } }
            }).then((fight) => {
                if (fight) {
                    return res.status(201).json({fight});
                }
            });
            // TODO - find a better opponent
            // Something like : where userId != userId
            // ORDER BY absolute(rankInititor-rankCharacter)
            // To have the closest opponent(s) in rank
            // Take all with same value then get a random with something like that
            // arrayOfCharacterWithSameRank[Math.floor(Math.random() * arrayOfCharacterWithSameRank.length)]
            const oneHourAgo = new Date(new Date().setHours(new Date().getHours()));
            Character.findOne({
                where: {
                    UserId: {
                        [Op.not]: userId
                    },
                    [Op.or]: [
                        { lastLoss: { [Op.gte]: oneHourAgo }},
                        { lastLoss: { [Op.eq]: null }}
                    ]
                    
                }
            }).then((opponent) => {
                if (opponent) {
                    let vals = {
                        initiator: initiator.id,
                        initiatorHP: initiator.dataValues.health,
                        opponent: opponent.id,
                        opponentHP: opponent.health,
                        round: 0,
                        logs: []
                    };
                    Fight.create(vals).then((newFight) => {
                        return res.status(201).json({
                            fight: newFight,
                            initiator,
                            opponent
                        });
                    }).catch((err) => {
                        console.error(err)
                        return res.status(500).json({'error': 'Can\'t create Fight'})
                    })
                } else {
                    return res.status(403).json({'error': 'character not found'});
                }
            }).catch((err) => {
                console.error(err);
                return res.status(500).json({'error': err});
            })
        } else {
            return res.status(403).json({'error': 'user not found'});
        }
    }).catch((err) => {
        console.error(err)
        return res.status(500).json({'error': 'Error server, please try again later'})
    })
});

// player1 attack player2
router.get('/attack/:fightId', async (req, res, next) => {
    const headerAuth = req.headers['authorization'];
    const userId = jwtUtils.getUserId(headerAuth);

    const fightId = req.params.fightId;

    if (userId < 0) {
        return res.status(403).json({'error': 'Incorrect token'});
    }

    const fight = await Fight.findOne({
        where: { id: fightId },
        include: [{
            model: Character,
            attributes: ['id', 'attack', 'defense', 'magik']
        }]
    });

    if (fight) {
        if (fight.winner) {
            return res.status(403).json({'error': 'Fight already over', fight});
        }

        const initiatorAttack = fight.round % 2 === 0;

        let playerId1 = fight.initiator;
        let playerId2 = fight.opponent;
        if (!initiatorAttack) {
            playerId1 = fight.opponent;
            playerId2 = fight.initiator;
        }
        const player1 = await Character.findOne({ where: { id: playerId1 } });
        const player2 = await Character.findOne({ where: { id: playerId2 } });

        const attackValue = parseInt(Math.random() * player1.attack);
        let log = '';

        // if attack value is greater than def it hit
        if (attackValue > player2.defense) {
            if (initiatorAttack) {
                log += `You make ${attackValue}dmg and hit!`
                // hit player2
                fight.opponentHP -= attackValue + player1.magik;
                // set winner
                if (fight.opponentHP <= 0) {
                    fight.opponentHP = 0;
                    fight.winner = fight.initiator;
                    player1.rank += 1;
                    if( player2.rank > 1) {
                        player2.rank -= 1;
                    }
                    player2.lastLoss = new Date();
                    player2.save();
                    player1.skillpoint += 1;
                    player1.save();
                    log += ` And you win GG!`
                }
            } else {
                log += `Enemy makes ${attackValue}dmg and hit D:`
                // hit player2
                fight.initiatorHP -= attackValue + player1.magik;
                // set winner
                if (fight.initiatorHP <= 0) {
                    fight.initiatorHP = 0;
                    fight.winner = fight.opponent;
                    player1.rank += 1;
                    if( player2.rank > 1) {
                        player2.rank -= 1;
                    }
                    player2.lastLoss = new Date();
                    player2.save();
                    player1.skillpoint += 1;
                    player1.save();
                    log += ` And you lose, nice try!`
                }
            }

            fight.round++;
            fight.logs = fight.logs.concat(log);
            fight.save().then((updateFight) => {
                if(updateFight){
                    return res.status(200).json(updateFight);
                } else {
                    return res.status(403).json({'error': 'Fight can\'t update'});
                }
            }).catch((err) => {
                console.error(err)
                return res.status(500).json({'error': err})
            });
        } else {
            if (initiatorAttack) {
                log += `You make ${attackValue}dmg but miss!`
            } else {
                log += `Enemy makes ${attackValue}dmg but miss!`
            }
            fight.round++;
            fight.logs = fight.logs.concat(log);
            fight.save().then((updateFight) => {
                if(updateFight){
                    return res.status(200).json(updateFight);
                } else {
                    return res.status(403).json({'error': 'Fight can\'t update'});
                }
            }).catch((err) => {
                console.error(err)
                return res.status(500).json({'error': err})
            });
        }
    } else {
        return res.status(403).json({'error': 'fight not found'});
    }
});

// list all fights of a character
router.get('/list/:characterId', (req, res, next) => {
    const characterId = req.params.characterId;

    Fight.findAll({
        where: { 
            [Op.or]: [
                { initiator: characterId },
                { opponent: characterId }
            ] }
    }).then((fights) => {
        return res.status(201).json(fights)
    }).catch((err) => {
        console.error(err);
        return res.status(500).json({'error': 'Error server, please try again later'})
    })
});

// list a specific fight
router.get('/details/:id', async (req, res, next) => {
    const headerAuth = req.headers['authorization'];
    const userId = jwtUtils.getUserId(headerAuth);

    const fightId = req.params.id;

    if (userId < 0) {
        return res.status(403).json({'error': 'Incorrect token'});
    }

    const fight = await Fight.findOne({ where: { id: fightId } })
    if(fight) {
        const initiator = await Character.findOne({ where: { id: fight.initiator } });
        const opponent = await Character.findOne({ where: { id: fight.opponent } });
        return res.status(201).json({fight, initiator, opponent});
    } else {
        return res.status(403).json({'error': 'fight not found'});
    }
});

// Recreate Fight's database
// FOR TESTING PURPOSES ONLY
router.get('/sync', function (req, res) {
    Fight.sync({force: true}).then(() => {
        return res.status(201).json({'Succes': "The table for the Fight model was just (re)created!"})
    }).catch((err) => {
        console.error(err);
        return res.status(500).json({'error': 'Error server, please try again later'})
    })
});

module.exports = router;