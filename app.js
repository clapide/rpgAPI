const path = require('path');
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const app = express();
const indexRouter = require('./routes');
const userRouter = require('./routes/users');
const characterRouter = require('./routes/characters');
const fightRouter = require('./routes/fights');

// view engine setup
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(cookieParser());
app.use('/public', express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', userRouter);
app.use('/characters', characterRouter);
app.use('/fights', fightRouter);

module.exports = app;
