'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Characters', [{
      skillpoint: 12,
      health: 10,
      attack: 10,
      defense: 7,
      magik: 2,
      rank: 1,
      UserId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Characters', [{
      skillpoint: 12,
      health: 16,
      attack: 0,
      defense: 0,
      magik: 0,
      rank: 1,
      UserId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Characters', null, {});
  }
};
