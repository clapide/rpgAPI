'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Fights', [{
      initiator: 1,
      initiatorHP: 10,
      opponent: 2,
      opponentHP: 16,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Fights', null, {});
  }
};
