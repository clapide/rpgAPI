'use strict';

const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let password = "azerty"
    // hash password before creating user
    let hash = bcrypt.hashSync(password, 5);

    await queryInterface.bulkInsert('Users', [{
      email: "mail@mail.fr",
      username: "user Name",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

    await queryInterface.bulkInsert('Users', [{
      email: "mail2@mail.fr",
      username: "user Name2",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
